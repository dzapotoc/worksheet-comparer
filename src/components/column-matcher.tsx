import { ReactElement, useEffect, useMemo, useState } from "react";
import { Button } from "react-bootstrap";
import './style.css';

export default function ColumnMatcher(props: ColumnMatcherProps): ReactElement {
    const { columnsOne, columnsTwo, onChange } = props;

    const [selectedPairs, setSelectedPairs] = useState<ColumnMatch[]>([]);
    const selectedOnes = useMemo(() => {
        return selectedPairs.map(pair => pair.tableOneColumn) ?? [];
    }, [selectedPairs]);
    const selectedTwos = useMemo(() => {
        return selectedPairs.map(pair => pair.tableTwoColumn) ?? [];
    }, [selectedPairs]);

    const [oneActive, setOneActive] = useState<string | null>(null);
    const firstTableColButtons = useMemo<ReactElement[]>(
        () => ColumnButtonPickerGroup(columnsOne, oneActive, setOneActive, selectedOnes),  
        [columnsOne, oneActive, setOneActive, selectedOnes]
    );

    const [twoActive, setTwoActive] = useState<string | null>(null);
    const secondTableColButtons = useMemo<ReactElement[]>(
        () => ColumnButtonPickerGroup(columnsTwo, twoActive, setTwoActive, selectedTwos),  
        [columnsTwo, twoActive, setTwoActive, selectedTwos]
    );

    useEffect(() => {
        if (oneActive && twoActive) {
            const pairs = [...selectedPairs, {tableOneColumn: oneActive, tableTwoColumn: twoActive}];
            setSelectedPairs(pairs);
            setOneActive(null);
            setTwoActive(null);
            onChange(pairs);
        }
    }, [oneActive, setOneActive, twoActive, setTwoActive, selectedPairs, setSelectedPairs, onChange]);

    const selectedPairsButtons = useMemo<ReactElement[]>(
        () => {
            return selectedPairs.map(pair => {
                const removePair = () => {
                    const pairs = [...selectedPairs.filter(p => pair.tableOneColumn !== p.tableOneColumn)];
                    setSelectedPairs(pairs);
                    onChange(pairs);

                };
                return <Button className='button' key={`${pair.tableOneColumn}${pair.tableTwoColumn}`} onClick={removePair}>
                    {`${pair.tableOneColumn} = ${pair.tableTwoColumn}`}
                </Button>;
            });
        }, [selectedPairs, setSelectedPairs, onChange]
    );

    return (
    <div>
        <div>
            <div className='buttons' style={{width: '50%', float: 'left', display: 'inline-block'}}> 
                <h6>Table 1 Columns</h6>
                <span>
                    { firstTableColButtons }
                </span>
            </div>
            <div className='buttons' style={{width: '50%', float: 'left', display: 'inline-block'}}> 
                <h6>Table 2 Columns</h6>
                <span>
                    { secondTableColButtons }
                </span>
            </div>
        </div>
        <div style={{textAlign: 'center'}}>
            <h6>Column Matches</h6>
            { selectedPairsButtons }
        </div>
    </div>
    );
}

export interface ColumnMatcherProps {
    columnsOne: string[];
    columnsTwo: string[];
    onChange(pairs: ColumnMatch[]): void;
}

export type ColumnMatch = {
    tableOneColumn: string;
    tableTwoColumn: string;
}

function ColumnButtonPickerGroup(
    columns: string[], 
    activeColumn: string | null,
    setActiveColumn: (colName: string) => void,
    selectedColumns: string[],
): ReactElement[] {
    return columns.map(col => {
        const toggleSelected = () => {
            if (activeColumn === col) {
                setActiveColumn(null);
            } else {
                setActiveColumn(col);
            }
        };
        const isDisabled = (activeColumn !== null && activeColumn !== col) || selectedColumns.includes(col);
        return <Button key={col} className='button' disabled={isDisabled} onClick={toggleSelected}>
            {col}
        </Button>;
    });
}
