import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import ExcelUploader from "./excel-uploader";

describe('ExcelUploader', () => {

    const file = new File(['foo'], 'foo.xlsx', {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
    const uploadCallbackMock = jest.fn;

    beforeEach(() => {
        render(<ExcelUploader title='Upload Test' onUpload={uploadCallbackMock}/>);
    })

    it('should render with a label from the title property', () => {
        expect(screen.getByLabelText(/Upload Test/u)).toBeInTheDocument();
    });

    it('uploads file', () => {
        const input = screen.getByLabelText(/Upload Test/u);
        userEvent.upload(input, file);
    });

});