import { ReactElement } from "react";
import { Form } from "react-bootstrap";
import { read, utils } from "xlsx";
import { DataRow, DataTable } from "../tables/table";
import './style.css';

export default function ExcelUploader(props: ExcepUploaderProps): ReactElement {
    const { title, onUpload } = props;

    async function upload(e: React.ChangeEvent<HTMLInputElement>) {
        if (e.target.files && e.target.files[0]) {
            try {
                const file = await e.target.files[0].arrayBuffer();
                const wb = read(file);
                const json = utils.sheet_to_json(wb.Sheets[wb.SheetNames[0]]);
                if (json && json.length) {
                    const columnNames = Object.keys(json[0]);
                    onUpload({
                        data: extractDataFromTable(json),
                        columns: columnNames,
                        selectedSheet: wb.Sheets[wb.SheetNames[0]],
                        workbook: wb
                    });
                }
            } catch (e) {
                console.error(e);
            }
        }
    };

    return (
        <div>
            <Form.Group controlId="formFileMultiple" className="mb-3">
                <Form.Label>{title}</Form.Label>
                <Form.Control type="file" onChange={upload}/>
            </Form.Group>
        </div>
    );
}

export interface ExcepUploaderProps {
    title: string;
    onUpload(data: DataTable): void;
}

function extractDataFromTable(table: unknown[]): DataRow[] {
    let index = 1;
    return table.map(row => {
        return { value: row, rowNum: ++index }
    });
}