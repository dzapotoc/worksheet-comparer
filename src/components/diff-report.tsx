import { ReactElement, useMemo } from "react";
import { useTable } from "react-table";
import { DiffInput, DiffRow } from "../tables/tableService";
import './style.css';

export default function DiffReport(props: {diffInput: DiffInput, diffRows: DiffRow[]}): ReactElement {
    const { diffInput, diffRows } = props;

    const columns = useMemo(
        () => {
            return diffInput.primary.columns.map(col => {
                return {
                    Header: col, accessor: col
                }
            });
        }, [diffInput]
    );

    const data = useMemo(() => {
        return diffRows.map(d => d.primaryRow.value as object);
    }, [diffRows]);

    const {
        getTableProps, 
        headerGroups, 
        getTableBodyProps, 
        rows, 
        prepareRow
    } = useTable({ columns, data});

    return (
        <div>
            <table {...getTableProps()}>
      <thead>
        {headerGroups.map(headerGroup => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map(column => (
              <th {...column.getHeaderProps()}>{column.render("Header")}</th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map((row, i) => {
          prepareRow(row);
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map(cell => {
                return <td {...cell.getCellProps()}>{cell.render("Cell")}</td>;
              })}
            </tr>
          );
        })}
      </tbody>
    </table>
        </div>
    );
}