import { ColumnMatch } from "../components/column-matcher";
import { DataRow, DataTable } from "./table";

export interface TableServiceBoundary {
    findDifferences(props: DiffInput): DiffRow[];
}

export type DiffInput = {
    primary: DataTable;
    secondary: DataTable;
    matchers: ColumnMatch[];
    checkers: ColumnMatch[];
}

export type DiffRow = {
    primaryRow: DataRow;
    secondaryRow: DataRow;
}

export class TableService implements TableServiceBoundary {
    findDifferences(props: DiffInput): DiffRow[] {
        const {primary, secondary, matchers, checkers} = props;

        const matchesKnownFields = buildComparator(matchers);
        const mismatchesUnsureFields = buildComparator(checkers, false);

        const differences: DiffRow[] = [];

        primary.data.forEach(primaryRow => {
            const errors = secondary.data
                .filter(secondaryRow => matchesKnownFields(primaryRow, secondaryRow))
                .filter(secondaryRow => mismatchesUnsureFields(primaryRow, secondaryRow));

            errors.forEach(secondaryRow => {
                differences.push({primaryRow, secondaryRow});
            });
        });

        return differences;
    }
}

function buildComparator(matchingFields: ColumnMatch[], trueOnEquals: boolean = true): (a: DataRow, b: DataRow) => boolean {
    return (a, b) => {
        let result = trueOnEquals;
        matchingFields.forEach(cond => {
            if (a.value[cond.tableOneColumn] !== b.value[cond.tableTwoColumn]) {
                result = !trueOnEquals;
            };
        });
        return result;
    };
}