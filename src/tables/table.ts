import { WorkBook, WorkSheet } from "xlsx";

export interface DataTable {
    columns: string[];
    data: DataRow[];
    selectedSheet: WorkSheet;
    workbook: WorkBook;
}

export interface DataRow {
    value: unknown;
    rowNum: number;
}