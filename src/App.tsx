import { useMemo, useState } from "react";
import ColumnMatcher, { ColumnMatch } from "./components/column-matcher";
import ExcelUploader from "./components/excel-uploader";
import { DataTable } from "./tables/table";
import './index.css';
import { Button } from "react-bootstrap";
import { DiffInput, DiffRow, TableService, TableServiceBoundary } from "./tables/tableService";
import DiffReport from "./components/diff-report";


const App = () => {
    const diffService: TableServiceBoundary = new TableService();

    const [primaryTable, setPrimaryTable] = useState<DataTable | undefined>();
    const [secondaryTable, setSecondaryTable] = useState<DataTable | undefined>();

    const [matchingPairs, setMatchingPairs] = useState<ColumnMatch[] | undefined>();
    const [checkingPairs, setCheckingPairs] = useState<ColumnMatch[] | undefined>();

    const remainingPrimaryColumnsAfterPicking = useMemo<string[]>(
        () => {
            if (primaryTable && matchingPairs) {
                const selectedTableColumns = matchingPairs.map(pair => pair.tableOneColumn);
                return findUnselectedColumns(primaryTable.columns, selectedTableColumns);
            }
        }, [primaryTable, matchingPairs]
    );
    const remainingSecondaryColumnsAfterPicking = useMemo<string[]>(
        () => {
            if (secondaryTable && matchingPairs) {
                const selectedTableColumns = matchingPairs.map(pair => pair.tableTwoColumn);
                return findUnselectedColumns(secondaryTable.columns, selectedTableColumns);
            }
        }, [secondaryTable, matchingPairs]
    );

    const comparatorInput = useMemo<DiffInput | undefined>(
        () => {
            if (checkingPairs) {
                return {
                    primary: primaryTable,
                    secondary: secondaryTable,
                    matchers: matchingPairs,
                    checkers: checkingPairs
                };
            }
        }, [primaryTable, secondaryTable, matchingPairs, checkingPairs]
    );

    const [report, setReport] = useState<DiffRow[] | undefined>();

    function executeDiffComparison() {
        setReport(diffService.findDifferences(comparatorInput));
    };

    return (<div className='background'>
        <div>
            <h1>Worksheet Comparer</h1>
            <br/>
            <ExcelUploader title="Select the first excel worksheet" onUpload={setPrimaryTable}/>
            <ExcelUploader title="Select the second excel worksheet" onUpload={setSecondaryTable}/>
        </div>

        {
            primaryTable && secondaryTable &&
            <div>
                <h3>Pair the Columns between the Worksheets</h3>
                <p>Select the columns that will be used to match rows between the two worksheets</p>
                <ColumnMatcher columnsOne={primaryTable.columns} columnsTwo={secondaryTable.columns} onChange={setMatchingPairs}/>
            </div>    
        }

        {
            matchingPairs && matchingPairs.length > 0 &&
            <div>
                <h3>Pair the Remaining Columns that should match</h3>
                <p>Select the columns that should match but could have errors</p>
                <ColumnMatcher 
                    columnsOne={remainingPrimaryColumnsAfterPicking} 
                    columnsTwo={remainingSecondaryColumnsAfterPicking} 
                    onChange={setCheckingPairs}
                />
            </div>
        }

        {
            comparatorInput &&
            <div>
                <p>Here is where you press the big button to find the difference!</p>
                <Button variant='danger' onClick={executeDiffComparison}>PRESS HERE</Button>
            </div>
        }

        {
            report &&
            <div>
                <DiffReport diffInput={comparatorInput} diffRows={report} />
            </div>
        }

    </div>);
};
  
export default App;

function findUnselectedColumns(allColumns: string[], selectedColumns: string[]): string[] {
    return allColumns.filter(col => !selectedColumns.includes(col));
}